const { ServiceBroker } = require('moleculer')

const broker = new ServiceBroker()

broker.createService({
    name: 'math',
    actions: {
        add(ctx) {
            return Number(ctx.params.a) + Number(ctx.params.b)
        }
    }
})

const params = { a: 5, b: 3 }

broker.start()
    .then(() => broker.call('math.add', params))
    .then(res => console.log(`${params.a} + ${params.b} = ${res}`))
    .catch(err => console.error(`Error occured! ${err.message}`))
    .then(() => {
        params.a = 12,
            params.b = 17
    })
    .then(() => broker.call('math.add', params))
    .then(res => console.log(`${params.a} + ${params.b} = ${res}`))
    .catch(err => console.error(`Error occured! ${err.message}`))
